import React, { Component } from "react";
import ShoeList from "./ShoeList";
import { shoeData } from "./data";
import ShoeDetail from "./ShoeDetail";
import Cart from "./Cart";

export default class ShoeShop extends Component {
  state = {
    shoeData: shoeData,
    shoeDetail: shoeData[0],
    cart: [],
  };

  handleViewDetail = (shoe) => {
    this.setState({ shoeDetail: shoe });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((item) => item.id == shoe.id);

    if (index == -1) {
      let newShoeArr = { ...shoe, soLuong: 1 };

      cloneCart.push(newShoeArr);
    } else {
      cloneCart[index].soLuong++;
    }

    this.setState({ cart: cloneCart });
  };

  handleRemove = (shoeID) => {
    let afterRemove = this.state.cart.filter((item) => item.id !== shoeID);

    this.setState({ cart: afterRemove });
  };

  handleQuantity = (shoeID, option) => {
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((item) => item.id == shoeID);

    cloneCart[index].soLuong += option;
    if (cloneCart[index].soLuong == 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };

  render() {
    return (
      <div className="container">
        <h1 className="text-center py-3">Shoe Shop</h1>
        <div className="row">
          <ShoeList
            shoeData={this.state.shoeData}
            handleViewDetail={this.handleViewDetail}
            handleAddToCart={this.handleAddToCart}
          />
          <Cart
            cart={this.state.cart}
            handleRemove={this.handleRemove}
            handleQuantity={this.handleQuantity}
          />
        </div>
        <ShoeDetail shoeDetail={this.state.shoeDetail} />
      </div>
    );
  }
}

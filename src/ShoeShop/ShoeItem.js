import React, { Component } from "react";

export default class ShoeItem extends Component {
  render() {
    let { shoeData, handleViewDetail, handleAddToCart } = this.props;
    let { image, name, price } = shoeData;
    return (
      <div className="col-4 mb-3">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt="shoe" />
          <div className="card-body">
            <h4 className="card-title mb-2">{name}</h4>
            <h5 className="text-success mb-3">{price}$</h5>
            <button
              onClick={() => {
                handleViewDetail(shoeData);
              }}
              className="btn btn-info"
            >
              Detail
            </button>
            <button
              onClick={() => {
                handleAddToCart(shoeData);
              }}
              className="btn btn-success mx-2"
            >
              Buy
            </button>
          </div>
        </div>
      </div>
    );
  }
}

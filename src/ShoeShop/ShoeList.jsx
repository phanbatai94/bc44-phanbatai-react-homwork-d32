import React, { Component } from "react";
import ShoeItem from "./ShoeItem";

export default class ShoeList extends Component {
  renderShoeList = () => {
    return this.props.shoeData.map((item) => {
      return (
        <ShoeItem
          shoeData={item}
          key={item.id}
          handleViewDetail={this.props.handleViewDetail}
          handleAddToCart={this.props.handleAddToCart}
        />
      );
    });
  };
  render() {
    return (
      <div className="col-6">
        <h2>Shoe List</h2>
        <div className="row">{this.renderShoeList()}</div>
      </div>
    );
  }
}
